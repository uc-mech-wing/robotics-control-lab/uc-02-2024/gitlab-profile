Coffee Cart Modifications
=========================

ENMT482 Students
----------------

The following instructions apply to students enrolled in _ENMT482_ who wish to
be able to use their own computing device to work on the manipulator laboratory
assignment.

1. Clone or copy
[__robodk-stations__](https://gitlab.com/uc-mech-wing/robotics-control-lab/uc-02-2024/robodk-stations)
to your device. This repository contains the _RoboDK_ station file
corresponding to the coffee cart apparatus to which you have been assigned.

2. Delete the other two station files to ensure they are not used by accident.

3. Clone or copy
[__modbus-client__](https://gitlab.com/uc-mech-wing/robotics-control-lab/uc-02-2024/modbus-client)
to your device. Use the _requirements.txt_ file from the repository to install
the necessary dependencies by following the instructions from the repository
_README_ file.

4. Copy _scale_client.py_ from [__modbus-client__](https://gitlab.com/uc-mech-wing/robotics-control-lab/uc-02-2024/modbus-client)
to the directory containing the Python executable.

It is expected that students will typically run their code using the Python
executable that is shipped with RoboDK:

    C:\RoboDK\Python-Embedded\python.exe

If you wish to run your code using a different Python executable to the one
listed above, be sure to _pip_ install _robodk_ first:

     C:\> pip install robodk

The _robodk_basics_ and _robodk_scales_ modules are provided as separate files
in the
[__robodk-stations__](https://gitlab.com/uc-mech-wing/robotics-control-lab/uc-02-2024/robodk-stations)
repository for students who wish to be able to run their code externally - that
is, from the command line (Windows) or Terminal (Linux). Both these modules are
also included within each _RoboDK_ station file for students who to prefer to
run their code from inside _RoboDK_.

Introduction
------------

__Coffee Cart Modifications__ contains select digital assets from project
_UC-02-2024_. The objective of this project is to upgrade the extant _ENMT482_
laboratory apparatus with a view to making the manipulator assignment a more
accurate reflection of the process by which high quality espresso is produced
in cafés. In order to ensure that students are able to consistently produce
espresso of an acceptable standard, the following modifications have been made
to the existing coffee cart apparatus.

1. A custom scale has been added below the dose hopper of the [Mazzer][mazzer]
Lux grinder of each coffee cart. This scale is built around a 1kg parallel beam
load cell that is wired to an HX711-based load cell amplifier. This in turn is
wired to a Raspberry Pi which hosts a Modbus Ethernet server daemon. The Mazzer
scale permits the weight of ground coffee dispensed from the dose hopper to be
measured within ± 0.1g.

2. A second custom scale has been added directly below the group head of the
[Rancilio][rancilio] Silvia espresso machine of each cart. The Rancilio scale
permits the weight of hot water dispensed from the group head to be measured
within ± 0.1g, which ensures that a 2:1 brew ratio is able to be consistently
achieved.

3. An [Auber][auber] PID controller with pre-infusion capability has been
fitted to the espresso machine of each cart. This unit provides superior
control of the water boiler temperature compared to the stock controller.

4. The standard Rancilio brass portafilters have been replaced with bottomless
stainless steel ones from [Normcore][normcore].  This has been done to permit
students to insert the portafilter into the group head of the espresso machine
using the [UR5][ur5] robot without risk of bending the portafilter - something
that was not possible before due to the brass ones being too easily damaged.

5. The standard Rancilio 58mm double-shot espresso baskets have been replaced
with higher-quality [VST][vst] 18g baskets. Along with the new bottomless
portafilters, this helps reduce the chances of shot channelling - or at least
makes channelling more apparent when it occurs.

6. A custom WDT (_Weiss Distribution Technique_) fixture has been added to each
cart to ensure that coffee grounds in the basket are distributed evenly with no
clumping prior to tamping. This fixture uses a set of acupuncture needles that
trace hypotrochoids in the coffee grounds much like the novel [Umikot][umikot]
WDT tool.

7. A [PUQ][puq] automatic tamping station has been added to each cart to ensure
that coffee grounds in the basket are tamped properly. The torque limits that
are imposed when the UR5 robot is operated in collaborative mode have prevented
this from being done before.

8. A custom cup dispensing fixture has been added to each cart to permit up to
50 espressos to be pulled back-to-back without the need for human intervention.

9. An automatic portafilter cleaning station has also been added to each cart -
again so that multiple espressos may be pulled back-to-back without the need
for human intervention.

Assignment
----------

Whilst the objective of the manipulator assignment is ostensibly to produce
high quality espresso using a UR5 robot, it is in reality a thinly-disguised
exercise in applied linear algebra. Specifically, the manipulator assignment
requires students to take points of interest that have been defined in one of
several local coordinate frames and transform them into the world frame. This
is achieved through the judicious use of HT (_Homogeneous Transformation_)
matrices and their constituent rotation matrices.

Students are expected to perform the aforementioned coordinate transformations
using Python. The resulting Python module will be situated within the item tree
of the [RoboDK][robodk] _station file_ that students are provided to use as the
basis of their assignment. These station files (also known as _RDK files_) are
specific to a particular robot / coffee cart combination.

In order to successfully complete the assignment, students must perform the
following tasks in less than ten minutes.

1. Beginning with the robot in the _Home_ position, attach the portafilter tool
to the robot and remove it from the tool stand.

2. Place the portafilter tool on the grinder scale pan.

3. Detach the portafilter tool from the robot.

4. Attach the grinder tool to the robot and remove it from the tool stand.

5. Using the grinder tool cushioning cylinder, unlock the grinder scale pan.

6. Using the grinder tool cushioning cylinder, press the grinder on button.

7. Wait 15 seconds.

8. Using the grinder tool cushioning cylinder, press the grinder off button.

9. Using the grinder tool dose bar, pull the grinder dose lever until the
grinder scale reports that 18g ± 0.1g of coffee has been dispensed.

10. Using the grinder tool cushioning cylinder, lock the grinder scale pan.

11. Return the grinder tool to the tool stand and detach it from the robot.

12. Attach the portafilter tool to the robot.

13. Remove the portafilter tool from the grinder scale pan.

14. Open the WDT fixture.

15. Place the portafilter tool in the WDT fixture.

16. Detach the portafilter tool from the robot.

17. Close the WDT fixture.

<del>18. Attach the grinder tool to the robot and remove it from the tool
stand.</del>

<del>19. Using the grinder tool cushioning cylinder, operate the WDT
fixture.</del>

<del>20. Return the grinder tool to the tool stand and detach it from the
robot.</del>

21. Open the WDT fixture.

22. Attach the portafilter tool to the robot.

23. Remove the portafilter tool from the WDT fixture.

24. Close the WDT fixture.

25. Insert the portafilter tool into the PUQ fixture.

26. Wait two seconds.

27. Remove the portafilter tool from the PUQ fixture.

28. Insert the portafilter tool into the espresso machine group head.

29. Lock the portafilter tool into the espresso machine group head.

30. Detach the portafilter tool from the robot.

31. Attach the grinder tool to the robot and remove it from the tool stand.

32. Using the grinder tool cushioning cylinder, open the cup dispenser fixture.

33. Using the grinder tool cushioning cylinder, close the cup dispenser
fixture.

34. Return the grinder tool to the tool stand and detach it from the robot.

35. Attach the cup tool to the robot and remove it from the tool stand.

36. Open the cup tool.

37. Using the cup tool, remove a cup from the cup dispenser.

38. Close the cup tool.

39. Place the cup on the espresso machine scale pan.

40. Open the cup tool.

41. Release the cup.

42. Close the cup tool.

43. Return the cup tool to the tool stand and detach it from the robot.

44. Attach the grinder tool to the robot and remove it from the tool stand.

45. Using the grinder tool cushioning cylinder, unlock the espresso machine
scale pan.

46. Using the grinder tool cushioning cylinder, turn the espresso machine brew
switch on.

47. Wait until the espresso machine scale reports that 36g ± 0.1g of water has
been dispensed.

48. Using the grinder tool cushioning cylinder, push the espresso machine brew
switch off.

49. Using the grinder tool cushioning cylinder, lock the espresso machine scale
pan.

50. Return the grinder tool to the tool stand and detach it from the robot.

51. Attach the cup tool to the robot and remove it from the tool stand.

52. Open the cup tool.

53. Pick up the cup.

54. Close the cup tool.

55. Position the cup tool in the customer zone.

56. Open the cup tool.

57. Release the cup.

58. Close the cup tool.

59. Return the cup tool to the tool stand and detach it from the robot.

60. Attach the portafilter tool to the robot.

61. Unlock the portafilter tool from the espresso machine group head.

62. Remove the portafilter tool from the espresso machine group head.

63. Position the portafilter tool over the portafilter cleaning fixture.

64. Operate the portafilter cleaning fixture using the portafilter tool.

65. Return the portafilter tool to the tool stand and detach it from the robot.

66. Attach the grinder tool to the robot and remove it from the tool stand.

67. Using the grinder tool cushioning cylinder, push the espresso machine brew
switch on.

68. Wait five seconds.

69. Using the grinder tool cushioning cylinder, push the espresso machine brew
switch off.

70. Return the grinder tool to the tool stand and detach it from the robot.

71. Return the robot to the _Home_ position.

Students may vary the order in which these tasks are performed within reason in
order to minimise the time taken to complete the full task list.

Students should also note that tasks 18 through 20 inclusive have been
temporarily replaced with a 15 second pause.

Warnings
--------

It is of __UTMOST IMPORTANCE__ that students use the station file corresponding
to the coffee cart apparatus to which they have been assigned. Failure to do so
will result in differences in behaviour when running offline (simulation only)
and online (virtual and physical robots synchronised). This is likely to result
in damage to both the robot and tooling due to variations in the location of
fixtures between carts.

In order to reduce the chances that students will select the incorrect station
file, the name of each cart is prominently displayed in multiple locations on
the cart itself. Station files have then been named after the coffee cart
apparatus to which they pertain.

| Cart Name | Station File     | Robot Details                |
| --------- | -----------------| ---------------------------- |
| Robot 1   | Robot_1_2024.rdk | UR5 serial number 2019350471 |
| Robot 2   | Robot_2_2024.rdk | UR5 serial number 2020350569 |
| Robot 3   | Robot_3_2024.rdk | UR5 serial number 2023350546 |

In addition to an accurate model of the coffee cart after which it is named,
each station file also contains a number of so-called _robot programs_.

Tool Programs
-------------

The first group of robot programs comprise those that students __MUST__ use to
perform certain real-world tasks, specifically:

 - Changing robot tools
 - Actuating robot tools
 - Actuating cart fixtures

This group of robot programs are located in the _Tool_Programs_ directory of
the RoboDK station tree.

If called when online - that is, when a socket connection has been established
between the laboratory computer running RoboDK and the physical robot - these
programs will result in synchronous changes to the state of the coffee cart
apparatus in both simulation and the real world. This includes movement of the
robot, actuation of cart fixtures, and so on. If called when offline, only the
state of the simulation will change.

Visual Programs
---------------

The second group of programs only change the state of the simulation, and have
no real-world effect whether they are called when online or offline. A classic
example of this type of program is the one that may be used to reset the state
of the simulation when things go awry.

This group of programs are located in the _Visual_Programs_ directory of the
RoboDK station tree.

Python Programs
---------------

In addition to robot programs, each station file also includes several example
Python programs. These are intended to provide students with a framework to use
as the basis for their own code, and also to serve as a gentle introduction to
the RoboDK [Python API][python_api].

This group of programs are located in the _Python_Programs_ directory of the
RoboDK station tree.

These Python programs may be called when online or offline, but some will have
a different effect when run on the laboratory computers directly connected to
the coffee carts. This includes the program that requests a Modbus Ethernet
server daemon to provide the current scale weight (grams), or to tare (zero)
the scale. It is only when called from a computer on the same network as the
scales that the daemon is able to respond to such requests. 

Documentation
-------------

Code has been documented using [Doxygen][doxygen].

License
-------

__Coffee Cart Modifications__ is released under the [GNU General Public
License][gpl].

Authors
-------

Code by Rodney Elliott, <rodney.elliott@canterbury.ac.nz>

[mazzer]: https://www.mazzer.com/en
[rancilio]: https://www.ranciliogroup.com
[auber]: https://www.auberins.com
[normcore]: https://www.normcorewares.com
[ur5]: https://www.universal-robots.com/products/ur5-robot
[vst]: https://store.vstapps.com
[umikot]: https://www.printables.com/model/481587-umikot-58mm-version-planetary-gear-spirograph-espr
[puq]: https://www.puqpress.com
[robodk]: https://robodk.com
[python_api]: https://robodk.com/doc/en/PythonAPI/index.html
[doxygen]: https://www.doxygen.nl
[gpl]: https://www.gnu.org/licenses/gpl-3.0.html
